import json
def load_api_key(filepath):
    """
    Because I do this so often
    :param filepath: the path to the file containing the API key
    :return: the API key
    """
    with open(filepath, 'r') as infile:
        return json.load(infile)['api_key']

def read_file(filepath):
    """
    Because I do this so often
    :param filepath: the path to the file to be read
    :return: the content of the file
    """
    with open(filepath, 'r') as infile:
        return infile.read()

def construct_prompt(template, keyword_map):
    """
    When I want to dynamically replace text in a prompt
    :param template: the template string to be modified
    :param keyword_map: a dictionary mapping keywords to their replacements
    :return: the modified template string
    """
    for key, value in keyword_map.items():
        template = template.replace(key, value)
    return template

def create_prompt(history, role, content):
    """
    appends the users message to the histoyr
    :param history: a list of prompts
    :param role: the role of the prompt
    :param content: the content of the prompt
    :return: the updated history with the new prompt added
    """
    prompt = {"role": role, "content": content}
    history.append(prompt)
    return history

def create_chat_completion(client, history, model, temperature, response_format):
    """
    The current way to get the chat completions
    :param client: the OpenAI client
    :param history: a list of chat history
    :param model: the model to be used for chat completion
    :param temperature: the temperature for chat completion
    :param response_format: the format of the response
    :return: the completed chat message
    """
    completion = client.chat.completions.create(
        model=model,
        messages=history,
        temperature=temperature,
        response_format=response_format
    )
    return completion.choices[0].message.content

def create_audio_file(client, text, output_file_path, model="tts-1", voice="nova",response_format="opus", speed="1"):
    """
    Generates an audio file from text using a specified TTS model and voice.

    :param client: An instance of the OpenAI client.
    :type client: OpenAI client object
    :param text: The text content to convert to audio.
    :type text: str
    :param output_file_path: The file path where the audio will be saved.
    :type output_file_path: str
    :param model: The TTS model to use for audio generation (default is "tts-1"), but there is also tts-1-hd.
    :type model: str
    :param voice: The voice to use for the generated audio (default is "nova"), alloy, echo, fable, onyx, and shimmer.
    :type voice: str
    :param response_format: The type of audio file mp3, opus, aac, and flac.
    :type voice: str
    :return: None
    """
    try:
        response = client.audio.speech.create(
            model=model,
            voice=voice,
            input=text
        )

        # Stream the audio to the specified file path
        response.stream_to_file(output_file_path)
    except Exception as e:
        print(f"Failed to generate audio. Error: {e}")


def create_image(client, prompt, model="dall-e-3", size="1024x1024", quality="standard", n=1):
   """
   Generates an image using a specified text prompt and DALL-E model.

   :param client: An instance of the DALL-E client.
   :type client: DALL-E client object
   :param prompt: The text prompt to generate the image from.
   :type prompt: str
   :param model: The DALL-E model to use for image generation (default is "dall-e-3").
   :type model: str
   :param size: The size of the generated image in the format "widthxheight" (default is "1024x1024").
   :type size: str
   :param quality: The quality setting for image generation (default is "standard").
   :type quality: str
   :param n: The number of images to generate (default is 1).
   :type n: int
   :return: A response containing the generated image(s).
   :rtype: DALL-E response object
   """

   response = client.images.generate(
       model=model,
       prompt=prompt,
       size=size,
       quality=quality,
       n=n,
   )
   return response

def download_image(image_url, file_name):
   """
   Downloads the png from a given URL and saves it to a specified file.

   :param image_url: The URL of the image to download.
   :type image_url: str
   :param file_name: The name of the file where the image will be saved.
   :type file_name: str
   :return: None
   """
   try:
       # Download the image and save it to a file
       urllib.request.urlretrieve(image_url, file_name)
       print(f"Image saved as {file_name}")
   except urllib.error.URLError as e:
       print(f"Failed to download image. Error: {e}")
